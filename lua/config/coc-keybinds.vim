" Coc find recent file
nnoremap <leader>fr :CocList mru<cr>

" Coc find buffer
nnoremap <leader>fb :CocList buffers<cr>

"nnoremap <leader>fg :CocList grep -e<space>

" Coc codeaction
nnoremap <leader>fa :CocList actions<cr>
nmap <leader>ac <Plug>(coc-codeaction)

" Coc diagnostics 
nnoremap <leader>fd :CocList diagnostics<cr>

" Coc outline
nnoremap <leader>fo :CocList outline<cr>

" Coc go to declaration
nmap <silent> <leader>gc <Plug>(coc-declaration)

" Coc go to definition
nmap <silent> <leader>gd <plug>(coc-definition)

" Coc grep word
nnoremap <silent> <leader>fw  :exe 'CocList grep '.expand('<cword>')<cr>

" Coc Eslint autofix
nnoremap <silent> <leader>lf :CocCommand eslint.executeAutofix<cr>

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

 function! s:show_documentation()
   if (index(['vim','help'], &filetype) >= 0)
     execute 'h '.expand('<cword>')
   else
     call CocAction('doHover')
   endif
 endfunction
