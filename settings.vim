" Setting files to be sourced
let files=[
  \'./plugins.vim',
  \'./keybinds.vim',
  \'./plugin-settings.vim',
  \'./plugin-settings.lua',
  \'./autocmd.lua',
  \'./general.lua',
\]

" Source each setting file
for file in files
  execute 'source' $SETTINGS_PATH . 'config/' . strpart(file, 1)
endfor
