# Vim Settings

The basic idea behind vim-settings is to sync Neovim settings in a git repository using [vim-plug](https://github.com/junegunn/vim-plug).
The solution is built with my settings embedded for others to use or fork and customize.
Once setup, any customizations and updates are synced via vim-plug.
In addition, as people fork the repo we can all take from each other and have a nice little place to see what others are doing.

Me use lua now, this need update.  Test.

### Install
- If you want to customize the settings then fork this repository first
- Install [HackNF](https://github.com/chrissimpkins/Hack#desktop-usage) font if using it
- Create/replace [init.vim](init.vim) file, updating [`settings_repo`](init.vim#L2) to your open Git repo if you forked the repository
    - Mac - `~/.config/nvim/init.vim`
    - Windows - `%LocalAppData%/nvim/init.vim`
- Open neovim and it should install everything automatically

### Pull Update
- Run `:PlugUpdate`
- Run `:so $MYVIMRC`
- If new plugins are added to [settings.vim](settings.vim), then run `:PlugInstall` and `:so $MYVIMRC`.

### Make Update
- You will need to have forked vim-settings to do this
- Make changes to vim-settings
    - You can open the vim-settings settings file with `:e $SETTINGS`
