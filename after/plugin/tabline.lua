local render = function(f)

	f.make_tabs(function(info)
		f.add(' ' .. info.index .. ' ')

		if info.filename then

			f.add(f.icon(info.filename) .. ' ')

			if string.match(info.filename, "index.[tj]sx?") then
				f.add(vim.fn.fnamemodify(info.buf_name, ":p:h:t") .. '/')
			end

			f.add(info.filename)
			f.add(info.modified and ' *')

			-- The icon function returns a filetype icon based on the filename
		else
			f.add(info.modified and '[*]' or '[-]')
		end
		f.add ' '
	end)
end


require('tabline_framework').setup {
	-- Render function is resposible for generating content of tabline
	-- This is the place where you do your magic!
	render = render,
	hl = { fg = "#abb2bf", bg = "#0d1117" },
	hl_sel = { fg = "#ffffff", bg = "#3182bd" },
}
