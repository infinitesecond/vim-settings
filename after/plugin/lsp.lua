local lsp = require('lsp-zero')
local util = require('lspconfig.util')

lsp.set_server_config({
  on_init = function(client)
    client.server_capabilities.semanticTokensProvider = nil
  end,
})

-- lsp.preset('recommended')
require('mason').setup({})
require('mason-lspconfig').setup({
  ensure_installed = {'ts_ls'},
	handlers =  {
		lsp.default_setup,
		denols = lsp.noop,
		ts_ls = lsp.noop
	}
})

lsp.on_attach(function(client, bufnr)
  -- see :help lsp-zero-keybindings
  -- to learn the available actions
  lsp.default_keymaps({buffer = bufnr})
end)


local function deno_tsls_root_dir(for_deno, prefer_deno)
	return function(fname)
		local deno = util.root_pattern("deno.json", "deno.jsonc")(fname)
		local tsls = util.root_pattern("package.json")(fname)
		-- print("deno root: ", deno)
		-- print("tsserver root: ", tsserver)

		if deno == nil then
			if for_deno then
				return nil
			end

			return tsls
		elseif tsls == nil then
			if not for_deno then
				return nil
			end

			return deno
		else
			-- Both found root dir files
			--
			if string.len(deno) < string.len(tsls) then
				-- Deno is most recent parent directory
				return deno
			elseif string.len(deno) > string.len(tsls) then
				-- Package json is most recent parent directory
				return tsls
			else
				-- both directories resolved are the same length (buggy maybe?)
				if prefer_deno then
					return deno
				else
					return tsls
				end
			end
		end
	end
end


lsp.configure('ts_ls', {
	root_dir = deno_tsls_root_dir(false, true),
	single_file_support = false,
	settings = {
		typescript = {
			importModuleSpecifierEnding = 'minimal'
		},
		javascript = {
			importModuleSpecifierEnding = 'minimal'
		},
	},
	on_init = function(client)
		client.server_capabilities.semanticTokensProvider = nil
	end
})

lsp.configure('denols', {
	root_dir = deno_tsls_root_dir(true, true),
})

local lua_opts = lsp.nvim_lua_ls({
	on_init = function(client)
		client.server_capabilities.semanticTokensProvider = nil
	end
})
require('lspconfig').lua_ls.setup(lua_opts)

--[[
lsp.configure('lua_ls',  {
  settings = {
    Lua = {
      runtime = {
        -- Tell the language server which version of Lua you're using
        -- (most likely LuaJIT in the case of Neovim)
        version = 'LuaJIT',
      },
      diagnostics = {
        -- Get the language server to recognize the `vim` global
        globals = {
          'vim',
          'require'
        },
      },
      workspace = {
        -- Make the server aware of Neovim runtime files
        library = vim.api.nvim_get_runtime_file("", true),
      },
      -- Do not send telemetry data containing a randomized but unique identifier
      telemetry = {
        enable = false,
      },
    },
  },
	on_init = function(client)
		client.server_capabilities.semanticTokensProvider = nil
	end
})
--]]

-- lsp.setup()
--
