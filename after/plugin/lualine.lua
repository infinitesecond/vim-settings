require('lualine').setup {
	options = {
		theme = 'base16',
		disabled_filetypes = {'NvimTree'}
	},
}
